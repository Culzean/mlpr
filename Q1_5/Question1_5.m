%% Neural Network with all pixels
rng(2019,'twister')
% Set up the network
tic
nhid = 10; % number of hidden units
net = mlp(size(xtr_nf,2), nhid, 1, 'linear');
% Set up vector of options
options = zeros(1,18);
options(1) = 1;
options(9) = 1;
options(14) = 200;
%for the optimiser.
% This provides display of error values.
% Check the gradient calculations.
% Number of training cycles.
% Train using scaled conjugate gradients.
[net, options] = netopt(net, options, xtr_nf(1:5000,:), ytr_nf(1:5000,:), 'scg');
toc
% RMSE on training set
ypred_tr = mlpfwd(net, xtr_nf);
rmse_NNsuball_tr = sqrt(mean(((ytr_nf - ypred_tr).^2)))
% RMSE on test set
ypred = mlpfwd(net, xte_nf);
rmse_NNsuball_te = sqrt(mean(((yte_nf - ypred).^2)))