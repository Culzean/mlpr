function [ patch ] = PlotPatch( patch )
%PLOTPATCH Summary of this function goes here
%   Detailed explanation goes here

patchPrint = [ patch zeros(1,18) ]
%display(patchPrint)
imagePatch = reshape(patchPrint, [35 30]);

imagesc(imagePatch');
colormap(gray(64));

end

