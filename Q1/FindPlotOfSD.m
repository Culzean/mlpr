function [ ret ] = FindPlotOfSD( patchesSTD, patches,SDTarget, aboveOrBelow )
%FINDPLOTOFSD Summary of this function goes here
%   Detailed explanation goes here

indices = [];

for i=1:size(patchesSTD),
    if(aboveOrBelow < 0) && (patchesSTD(i) <= SDTarget),
       indices = [indices i];
    elseif(aboveOrBelow > 0) && (patchesSTD(i) >= SDTarget),
       indices = [indices i];
    end
end

randIndices = datasample(indices,1)
ret = patches(randIndices,:); 

end

