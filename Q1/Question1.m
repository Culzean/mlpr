patchSTD = std(xtr./63, 0, 2);
%histogram(patchSTD, 64)

%trXNorm = xtr ./ 63

below = FindPlotOfSD( patchSTD, xtr, 0.22, -1 );

PlotPatch(below)        
sampleSize = 10000;

leftpixels = zeros(1,sampleSize);
abovepixels = zeros(1,sampleSize);
targetpixels = zeros(1,sampleSize);

what = xtr_nf(1, end );

for i=1:sampleSize;
    sample = randi(size(ytr_nf,1),1);

    leftpixels(sample) = xtr_nf(sample, end );
    abovepixels(sample) = xtr_nf(sample, end-34);
    targetpixels(sample) = ytr_nf(sample);
end;

%Plot3D(leftpixels, abovepixels, targetpixels)

weights = ones(size(ytr_nf,1), 1);
X = cat(2, xtr_nf(:, end), xtr_nf(:, end-34), weights );

%don't know what to do with this?
OLS = inv(X.' * X) * X.' * ytr_nf;

yhat = X * OLS

x1 = xte_nf(:, end )
x2 = xte_nf(:, end-34)



Plot3D(x1, x2, yte_nf, yhat, OLS);

RSS = ytr_nf.' * ytr_nf - ytr_nf.' * X * OLS;

MSE = sum(RSS) / size(ytr_nf, 1)
RMSE = sqrt(MSE)

rmse_NNsuball_tr = sqrt(mean(((ytr_nf - yhat).^2)))

%RMSE = sqrt(RSS)

%tbl = table(xtr_nf(:, end), xtr_nf(:, end-34))
%lm = fitlm(tbl)

