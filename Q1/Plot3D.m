function [  ] = Plot3D( leftpixels, abovepixels, targetpixels, yhat, OLS )
%PLOT3D Summary of this function goes here
%   Detailed explanation goes here
figure,
scatter3(leftpixels, abovepixels, targetpixels, 'x', 'r' )
%caxis([0 30])
%line( leftpixels, yhat, abovepixels );

hold on
[dim1, dim2] = meshgrid(0:0.01:1, 0:0.01:1 );
ysurf = [[dim1(:), dim2(:)], ones(numel(dim1),1)]*OLS;
surf(dim1, dim2, reshape(ysurf, size(dim1)))
end

