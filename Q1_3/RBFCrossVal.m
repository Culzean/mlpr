function [ RMSE, yHat ] = RBFCrossVal( X_Train, X_Test )
%RBFCROSSVAL Summary of this function goes here
%   Detailed explanation goes here

%create and train RBF over the training set
%then return RMSE calculated on the test set
nbf = 5;
dim = (size( X_Train, 2 ) - 1);
net = rbf( dim, nbf, 1, 'gaussian', 'linear' );

options = foptions;
options(1) = 1;
options(14) = 5; %number of iterations
Y_Train_Target = X_Train( :, end )
X_Train_Input = X_Train( :, 1:2 );
rbftrain( net, options, X_Train_Input, Y_Train_Target );

Y_Test_Target = X_Test( :, end )
X_Test_Input = X_Test( :, 1:2 );

yHat = rbffwd(net, X_Test_Input);

RMSE = sqrt( mean(Y_Test_Target - yHat).^2 );

end

