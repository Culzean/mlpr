function [ prob ] = FindProbability( y, fittedWeights, x )
%FINDPROBABILITY Summary of this function goes here
%   Detailed explanation goes here

prob = 1 ./ ( 1 + exp(-y .* fittedWeights.' * x) );

end

