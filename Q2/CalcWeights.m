function [ weightX ] = BiasWeights( data_x )
%CALCWEIGHTS Summary of this function goes here
%   Detailed explanation goes here

biasVals_X = ones(size(data_x,1),1)

weightX = [data_x, biasVals_X]

end

