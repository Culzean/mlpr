
load('text_data.mat')

[x_test_bias] = BiasWeights(x_test);

x_train_bias = BiasWeights(x_train);

weights = ones(size(x_test_bias,2),1);

[lp dpl_dw] = lr_loglike( weights ,x_train_bias, y_train )

myFunwrapper = @(weightsChop ,x_train, y_train) neg_lr_loglike(weightsChop ,x_train, y_train);

%how do we turn this on its head for a mle?
[fittedWeights, progress, searchCount] = minimize( weights, myFunwrapper, 1000 ,x_train_bias, y_train )

probability = FindProbability( +1, fittedWeights, x_train_bias' );
weights = ones(size(x_test_bias,2)+1,1);
meanLogProb = mean(probability)

classCountNeg = find(probability> 0.5); 
classCountPos = find(probability<= 0.5);

mostLikely = max( size(classCountNeg), size(classCountPos) )

accuracy = mostLikely / size(probability)

errorbar_str(probability)

correct = 0
for i =1:size(y_train);
    y1 = probability(i);
    y0 = 1 - y1;
   if( y1 > y0 && y_train(i) < 0);
        correct = correct +1;
   end;
   if(y1 > y0 && y_train(i) > 0);
        correct = correct +1;
   end;
    
end

[ probability' y_train ]

accuracy = correct / size(probability, 2)

errorbar_str(probability)

% find the mle for the test set
%probabilityTest = FindProbability( +1, fittedWeights, x_train_bias );


%classCountNeg = find(probabilityTest> 0.5); 
%classCountPos = find(probabilityTest<= 0.5);

%mostLikelyTest = max( size(classCountNeg), size(classCountPos) )

%accuracy = mostLikelyTest / size(probabilityTest)

%errorbar_str(probabilityTest)
