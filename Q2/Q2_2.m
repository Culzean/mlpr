
load('text_data.mat')

checkNoisyLogFunc = @(weights,x_train, y_train) lr_loglike_noisy(weights,x_train, y_train);


[x_test_bias] = BiasWeights(x_train);

epsilon = 0.5*ones(size(x_test_bias,2)+1,1);

weights = ones(size(x_test_bias,2)+1,1);
weights(end-1) = 0.4;

%y_train = 1;
%x_test_bias = [1,1];
%epsilon = [0;0;0];

%[lp, dpl_de] = lr_loglike_noisy( weightsChop, epsilon ,x_train, y_train );

%checkgrad( checkNoisyLogFunc, epsilon, 0.00001, x_test_bias, y_train )

%[fittedWeights, progress, searchCount] = minimize( weightsChop, myFunwrapper, 100 ,x_train, y_train )

NLLFunc = @(weights ,x_test_bias, y_train) NLL_noisy(weights ,x_test_bias, y_train);

%NLL_noisy(weights ,x_train, y_train)

%how do we turn this on its head for a mle?
[fittedWeights, progress, searchCount] = minimize( weights, NLLFunc, 500 ,x_test_bias, y_train )

temp = zeros( size(x_test_bias(),1), 1);
x_test_bias = [x_test_bias temp];
probability = FindProbability( +1, fittedWeights, x_test_bias' );
probability = probability';
meanLogProb = mean(probability)

correct = 0
for i =1:size(y_train);
    y1 = probability(i);
    y0 = 1 - y1;
   if( y1 < y0 && y_train(i) < 0);
        correct = correct +1;
   end;
   if(y1 > y0 && y_train(i) > 0);
        correct = correct +1;
   end;
    
end

accuracy = correct / size(probability, 1)

errorbar_str(probability)
