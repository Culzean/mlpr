function [ Lp, dLp_dw ] = neg_lr_loglike_noise( ww, xx, yy )
%LR_LOGLIKE log-likelihood and gradients of logistic regression
%
%     [Lp, dLp_dw] = lr_loglike(ww, xx, yy);
%
% Inputs:
%          ww Dx1 logistic regression weights
%          xx NxD training data, N feature vectors of length D
%          yy Nx1 labels in {+1,-1} or {1,0}
%
% Outputs:
%          Lp 1x1 negative log-probability of data, the nagative log-likelihood of ww
%      dLp_dw Dx1 gradients: partial derivatives of Lp wrt ww

% Iain Murray, October 2014, August 2015

% Ensure labels are in {+1,-1}:
yy = (yy==1)*2 - 1;

sigmas = 1./(1 + exp(-yy.*(xx*ww))); % Nx1
Lp = -1 * sum(log(sigmas));

if nargout > 1
    dLp_dw = -1 * (((1-sigmas).*yy)' * xx)';


end